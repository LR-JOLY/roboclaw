# roboclaw

C++ ROS driver for Roboclaw


## Installation
You have just to clone the repo in your catkin_ws and to compile it
'''
cd catkin_ws/src
git clone https://gitlab.com/LR-JOLY/roboclaw.git
cd ..
catkin_make
'''
## Hardware
You need a roboclaw driver, connected to a power source. USB is not suffisant, even if you just want to connect to the device.
We recommand you to create a udev rule to access more easily to the device.
Example :
In **/etc/udev/rules.d** create a file **99-roboclaw.rules**
with this content 
'''
KERNEL=="ttyACM0" , ATTRS{idProduct}=="2404", ATTRS{idVendor}=="03eb", MODE="666", SYMLINK+="roboclaw"
'''
## License
TBD

## Project status
project is on going

#ifndef ROBOCLAW_HPP
#define ROBOCLAW_HPP

#include <stdint.h> //uint8_t, int16_t
#include <string> //string
#include <unistd.h> //read, close
#include <fcntl.h> //O_RDWR file open flag
#include <termios.h> //struct termios, tcgetattr, tcsetattr, cfsetispeed, tcflush
#include <assert.h> //assert
#include <sys/select.h> //select
#include <malloc.h> //malloc, free
#include <errno.h> //errno
#include <exception>
#include <iostream>
#include <vector>

using std::string;
using std::exception;
using std::cout;
using std::endl;
using std::abs;

#define ROBOCLAW_BUFFER_SIZE 128



/**
 * @class       Roboclaw roboclaw.hpp "roboclaw.h"
 * @brief       declaration of the Roboclaw class 
 * @details     The \c Roboclaw class is the \em ROS \em driver for \b Roboclaw motor driver
 * @author      Louis-Romain JOLY <louis-romain.joly@sncf.fr>
 * @version     0.0
 * @date        2021
 * @note        TBD
 * @pre         TBD
 * @post        TBD
 * @bug         TBD
 * @warning     A bad use may distroy your hardware
 * @attention   TBD
 * @remark      TBD
 * @copyright   GNU Public License.
 */
class Roboclaw
{
    public:

        Roboclaw();
        Roboclaw(const char* com_port, int baud_rate);
        Roboclaw(const char* com_port, int baud_rate, int retries, int strict_0xFF_ACK);
        ~Roboclaw();
        int get_status();
        int displayVersion(uint8_t address);
        int driveMotor(uint8_t address, uint8_t motor, int8_t value);
        int setBatteryLimitVoltage(uint8_t address, uint8_t battery, uint8_t type, uint8_t value);
        int driveMotor7bit(uint8_t address, uint8_t motor, uint8_t value);
        int readEncoder(uint8_t address, uint8_t motor, uint32_t *encoder, uint8_t *status);
        int readEncoderSpeed(uint8_t address, uint8_t motor, uint32_t *encoder_speed, uint8_t *status);
        int readRawSpeed(uint8_t address, uint8_t motor, uint32_t *raw_speed, uint8_t *status);
        int resetEncoders(uint8_t address);
        int setEncoderValue(uint8_t address, uint8_t motor, uint32_t value);
        int readRawSpeed(uint8_t address, uint8_t motor, uint32_t *raw_speed);
        int readBatteryLevel(uint8_t address, uint8_t battery, uint16_t *voltage);
        int setBatteryLimitVoltages(uint8_t address, uint8_t battery, uint16_t min_voltage, uint16_t max_voltage);
        int setVelocityPIDConstants(uint8_t address, uint8_t motor, uint32_t P, uint32_t I, uint32_t D, uint32_t QPPS);
        int driveMotorWithDutyCycle(uint8_t address, uint8_t motor, int16_t value);
        int driveMotorsWithDutyCycle(uint8_t address, int16_t duty_M1, int16_t duty_M2);
        int driveMotorWithSpeed(uint8_t address, uint8_t motor, int32_t speed);
        int driveMotorsWithSpeed(uint8_t address, int32_t speed_M1, int32_t speed_M2);
        int driveMotorWithSpeedAndAccel(uint8_t address, uint8_t motor, int32_t speed, int32_t accel);
        int driveMotorsWithSpeedAndAccel(uint8_t address, int32_t speed_M1, int32_t speed_M2, int32_t accel);
        int driveMotorWithSpeedAndDist(uint8_t address, uint8_t motor, int32_t speed, int32_t distance, int8_t buffer);
        int driveMotorsWithSpeedAndDist(uint8_t address, int32_t speed_M1, int32_t distance_M1, int32_t speed_M2, int32_t distance_M2, int8_t buffer);
        int driveMotorWithSpeedAccelAndDist(uint8_t address, uint8_t motor, int32_t speed, int32_t accel, int32_t distance, int8_t buffer);
        int driveMotorsWithSpeedAccelAndDist(uint8_t address, int32_t speed_M1, int32_t distance_M1, int32_t speed_M2, int32_t accel,  int32_t distance_M2, int8_t buffer);
        int readVelocityPID(uint8_t address, uint8_t motor, std::vector<uint32_t> &PID);
        int readBatteryLimitVoltages(uint8_t address, uint8_t battery, uint16_t *limit_voltages[2]);
        void closeCommunication(void);
        int readEncoder(uint8_t address, std::vector<uint32_t> &encoders, std::vector<uint8_t> &encoders_status);
        int readEncoderSpeed(uint8_t address, std::vector<uint32_t> &encoders_speed, std::vector<uint8_t> &encoders_status);

        
    private:
        // PARAM
        const char *com_port_; //!< communication port of the roboclaw (tty...)
        int baud_rate_; //!< rate at which information is transferred between computer and roboclaw.
        int timeout_ms_; //!< timeout in ms for the roundtrip sending command and waiting for ACK or response
        int retries_; //!< number of retries the library should make before reporting failure (both timeout and incorrect crc)
        int strict_0xFF_ACK_; //!< require strict 0xFF ACK byte matching (treat non 0xFF as crc failure)
        //int device_handle_; //!< roboclaw file handle for termios
        int status_; //!< roboclaw status (0 is OK, -1 otherwise)
        struct termios initial_termios_;
	    struct termios actual_termios_;
	    unsigned char buffer_[ROBOCLAW_BUFFER_SIZE];
        // METHOD
        void setTimeout();
        speed_t getSpeed();
        int openTermios();
        uint16_t decodeUint16(unsigned char *buffer);
        uint32_t decodeUint32(unsigned char *buffer);
        unsigned int updateCrc(unsigned char *packet, int nBytes, unsigned int crc);
        int write_(uint8_t address, uint8_t command);
        int write_1(uint8_t address, uint8_t command, uint8_t value);
        int write_2(uint8_t address, uint8_t command, uint16_t value);
        int write_2x2(uint8_t address, uint8_t command, int16_t values[2]);
        int write_2x2u(uint8_t address, uint8_t command, uint16_t values[2]);
        int write_4(uint8_t address, uint8_t command, uint32_t value);
        int write_2x4(uint8_t address, uint8_t command, int32_t values[2]);
        int write_2x41(uint8_t address, uint8_t command, int32_t values[2], int8_t value);
        int write_3x4(uint8_t address, uint8_t command, int32_t values[3]);
        int write_3x41(uint8_t address, uint8_t command, int32_t values[3], int8_t value);
        int write_4x4(uint8_t address, uint8_t command, uint32_t values[4]);
        int write_4x41(uint8_t address, uint8_t command, int32_t values[2], int8_t value);
        int write_5x41(uint8_t address, uint8_t command, int32_t values[5], int8_t value);
        int writeCmdChecksum(unsigned int bytes);
        int read_2(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint16_t *value);
        int read_2x2(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint16_t *values[2]);
        int read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t *value);
        int read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t *value, uint8_t *status);
        int read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t &value, uint8_t &status);
        int read_4x4(uint8_t address, uint8_t command, uint8_t bytes_to_read, std::vector<uint32_t> &values);
};





/**
 * @class       Roboclaw roboclaw.hpp "roboclaw.hpp"
 * @brief       declaration of the Roboclaw exception class 
 * @details     The \c Roboclaw \c exceptionclass is the \em ROS \em driver for \b Roboclaw motor driver
 * @author      Louis-Romain JOLY <louis-romain.joly@sncf.fr>
 * @version     0.0
 * @date        2021
 * @note        TBD
 * @pre         TBD
 * @post        TBD
 * @bug         TBD
 * @warning     A bad use may distroy your hardware
 * @attention   TBD
 * @remark      TBD
 * @copyright   GNU Public License.
 */
class RoboclawError: public exception
{
public:
    RoboclawError(int number=0, string const& text="", int level=0) throw()
         :error_number_(number),error_text_(text),error_level_(level)
    {}
 
     virtual const char* what() const throw()
     {
         return error_text_.c_str();
     }
     
     int get_level() const throw()
     {
          return error_level_;
     }
    
    virtual ~RoboclawError() throw()
    {}
 
private:
    int error_number_;               //error number
    string error_text_;            //error cause
    int error_level_;               //error level (0 is fatal)
};

#endif // ROBOCLAW_HPP

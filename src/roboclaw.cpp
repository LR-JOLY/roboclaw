#include "roboclaw/roboclaw.hpp"

//crc sizes, reply sizes
enum { ROBOCLAW_CRC16_BYTES=2,
 ROBOCLAW_READ_MAIN_BATTERY_REPLY_BYTES=4, ROBOCLAW_READ_ENCODERS_REPLY_BYTES=7,
 ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES=7,ROBOCLAW_READ_VERSION=50,
 ROBOCLAW_READ_SPEED_PID_REPLY_BYTES=18, ROBOCLAW_READ_LIMIT_VOLTAGES_REPLY_BYTES=6};

/**
 * @enum roboclaw status values enumeration
 * @brief names are explicit
 *
 */
enum {ROBOCLAW_ERROR=-1, ROBOCLAW_RETRIES_EXCEEDED=-2, ROBOCLAW_OK=0};


/**
 * @enum roboclaw default library values enumeration
 * @brief see Roboclaw documentation
 *
 */
enum {  ROBOCLAW_DEFAULT_RETRIES=3,
        ROBOCLAW_DEFAULT_STRICT_0XFF_ACK=0,
        ROBOCLAW_B2400_TIMEOUT_MS=100,
        ROBOCLAW_B9600_TIMEOUT_MS=30,
        ROBOCLAW_B19200_TIMEOUT_MS=20,
        ROBOCLAW_B38400_TIMEOUT_MS=15,
        ROBOCLAW_B57600_TIMEOUT_MS=13,
        ROBOCLAW_B115200_ABOVE_TIMEOUT_MS=12};


/**
 * @enum roboclaw command enumeration
 * @brief see Roboclaw documentation
 *
 */
enum {		M1FORWARD = 0,
			M1BACKWARD = 1,
			SETMINMB = 2,
			SETMAXMB = 3,
			M2FORWARD = 4,
			M2BACKWARD = 5,
			M17BIT = 6,
			M27BIT = 7,
			MIXEDFORWARD = 8,
			MIXEDBACKWARD = 9,
			MIXEDRIGHT = 10,
			MIXEDLEFT = 11,
			MIXEDFB = 12,
			MIXEDLR = 13,
			SETSERIALTIMEOUT = 14,
			READSERIALTIMEOUT = 15,
			GETM1ENC = 16,
			GETM2ENC = 17,
			GETM1SPEED = 18,
			GETM2SPEED = 19,
			RESETENC = 20,
			GETVERSION = 21,
			SETM1ENCCOUNT = 22,
			SETM2ENCCOUNT = 23,
			GETMBATT = 24,
			GETLBATT = 25,
			SETMINLB = 26,
			SETMAXLB = 27,
			SETM1PID = 28,
			SETM2PID = 29,
			GETM1ISPEED = 30,
			GETM2ISPEED = 31,
			M1DUTY = 32,
			M2DUTY = 33,
			MIXEDDUTY = 34,
			M1SPEED = 35,
			M2SPEED = 36,
			MIXEDSPEED = 37,
			M1SPEEDACCEL = 38,
			M2SPEEDACCEL = 39,
			MIXEDSPEEDACCEL = 40,
			M1SPEEDDIST = 41,
			M2SPEEDDIST = 42,
			MIXEDSPEEDDIST = 43,
			M1SPEEDACCELDIST = 44,
			M2SPEEDACCELDIST = 45,
			MIXEDSPEEDACCELDIST = 46,
			GETBUFFERS = 47,
			GETPWMS = 48,
			GETCURRENTS = 49,
			MIXEDSPEED2ACCEL = 50,
			MIXEDSPEED2ACCELDIST = 51,
			M1DUTYACCEL = 52,
			M2DUTYACCEL = 53,
			MIXEDDUTYACCEL = 54,
			READM1PID = 55,
			READM2PID = 56,
			SETMAINVOLTAGES = 57,
			SETLOGICVOLTAGES = 58,
			GETMINMAXMAINVOLTAGES = 59,
			GETMINMAXLOGICVOLTAGES = 60,
			SETM1POSPID = 61,
			SETM2POSPID = 62,
			READM1POSPID = 63,
			READM2POSPID = 64,
			M1SPEEDACCELDECCELPOS = 65,
			M2SPEEDACCELDECCELPOS = 66,
			MIXEDSPEEDACCELDECCELPOS = 67,
			SETM1DEFAULTACCEL = 68,
			SETM2DEFAULTACCEL = 69,
			SETPINFUNCTIONS = 74,
			GETPINFUNCTIONS = 75,
			SETDEADBAND	= 76,
			GETDEADBAND	= 77,
			GETENCODERS = 78,
			GETISPEEDS = 79,
			RESTOREDEFAULTS = 80,
			GETTEMP = 82,
			GETTEMP2 = 83,	//Only valid on some models
			GETERROR = 90,
			GETENCODERMODE = 91,
			SETM1ENCODERMODE = 92,
			SETM2ENCODERMODE = 93,
			WRITENVM = 94,
			READNVM = 95,	//Reloads values from Flash into Ram
			SETCONFIG = 98,
			GETCONFIG = 99,
			SETM1MAXCURRENT = 133,
			SETM2MAXCURRENT = 134,
			GETM1MAXCURRENT = 135,
			GETM2MAXCURRENT = 136,
			SETPWMMODE = 148,
			GETPWMMODE = 149,
			FLAGBOOTLOADER = 255};	//Only available via USB communications


namespace
{
    int device_handle_; //!< roboclaw file handle for termios
}


/**
 * @brief Default constructor of the Roboclaw class.
 *
 * @see   Roboclaw::Roboclaw(...)
 */
Roboclaw::Roboclaw()
{

}


/**
 * @brief Default destructor of the Roboclaw class.
 *
 * @see   Roboclaw::Roboclaw(...)
 */
Roboclaw::~Roboclaw()
{
	cout << "Destruction" << endl;
	try
    {
        if (tcsetattr(device_handle_, TCSANOW, &initial_termios_) < 0)
        {
            throw RoboclawError(5,"unable to set termios structure to initial values",0);
        }
        if (close(device_handle_) < 0)
        {
            throw RoboclawError(6,"unable to close communication",0);
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return;
    }

}


/**
 * @brief Constructor of the Roboclaw class.
 * @overload
 * @param comp_port communication port of the roboclaw (eg. dev/ttyACM0...)
 * @param baud_rate rate to choose in the following list (2400, 9600, 19200, 38400, 57600, 115200, 230400, 460800)
 * 
 * @see   Roboclaw::Roboclaw(...)
 */
Roboclaw::Roboclaw(const char *com_port, int baud_rate)
{
	com_port_ = com_port;
    baud_rate_ = baud_rate;
    setTimeout();
    retries_ = ROBOCLAW_DEFAULT_RETRIES;
    strict_0xFF_ACK_ = ROBOCLAW_DEFAULT_STRICT_0XFF_ACK;

    status_ = openTermios();

}

/**
 * @brief Constructor of the Roboclaw class.
 * @overload
 * @param comp_port communication port of the roboclaw (eg. dev/ttyACM0...)
 * @param baud_rate rate to choose in the following list (2400, 9600, 19200, 38400, 57600, 115200, 230400, 460800)
 * @param timeout_ms timeout in ms for the roundtrip sending command and waiting for ACK or response
 * @param retries number of retries the library should make before reporting failure (both timeout and incorrect crc)
 * @param strict_0xFF_ACK require strict 0xFF ACK byte matching (treat non 0xFF as crc failure)
 * 
 */
Roboclaw::Roboclaw(const char *com_port, int baud_rate, int retries, int strict_0xFF_ACK)
{

	com_port_ = com_port;
    baud_rate_ = baud_rate;
    setTimeout();
    retries_ = retries;
    strict_0xFF_ACK_ = strict_0xFF_ACK;

    status_ = openTermios();

}

/**
 * @brief Method to set timeout_ms_ parameter
 *  
 */
void Roboclaw::setTimeout()
{
    if(baud_rate_ == 2400) timeout_ms_=ROBOCLAW_B2400_TIMEOUT_MS;
	else if(baud_rate_ == 9600) timeout_ms_=ROBOCLAW_B9600_TIMEOUT_MS;
	else if(baud_rate_ == 19200) timeout_ms_=ROBOCLAW_B19200_TIMEOUT_MS;
	else if(baud_rate_ == 38400) timeout_ms_=ROBOCLAW_B38400_TIMEOUT_MS;
	else if(baud_rate_ == 57600) timeout_ms_=ROBOCLAW_B57600_TIMEOUT_MS;
	else timeout_ms_=ROBOCLAW_B115200_ABOVE_TIMEOUT_MS;
}

/**
 * @brief Method to get the transmission speed
 * 
 * @param baud_rate rate to choose in the following list (2400, 9600, 19200, 38400, 57600, 115200, 230400, 460800)
 * 
 */
speed_t Roboclaw::getSpeed()
{
    speed_t speed=0;

	if(baud_rate_ == 2400) speed=B2400;
	else if(baud_rate_ == 9600) speed=B9600;
	else if(baud_rate_ == 19200) speed=B19200;
	else if(baud_rate_ == 38400) speed=B38400;
	else if(baud_rate_ == 57600) speed=B57600;
	else if(baud_rate_ == 115200) speed=B115200;
	else if(baud_rate_ == 230400) speed=B230400;
    //B460800 is non-standard but is defined on modern systems
    #ifdef B460800
	    if(baud_rate_ == 460800) speed=B460800;
    #endif

    return speed;
}

/**
 * @brief Method to open communication through termios
 * 
 */
int Roboclaw::openTermios()
{
    speed_t speed = getSpeed();
    device_handle_ = open(com_port_, O_RDWR | O_NOCTTY);
	//device_handle_ = open(com_port_, O_RDWR | O_NOCTTY | O_NDELAY);

    try
    {
        if (speed == 0)
        {
            throw RoboclawError(0,"baud rate is incorrect",0);
        }

        if (device_handle_ == -1)
        {
            throw RoboclawError(1,"unable to open roboclaw port",0);
        }
        if (tcgetattr(device_handle_, &initial_termios_) < 0)
        {
            throw RoboclawError(2,"unable to get the termios structure",0);
        }

        actual_termios_.c_iflag = actual_termios_.c_oflag = actual_termios_.c_lflag = 0;
        actual_termios_.c_cflag = CS8|CREAD|CLOCAL; //8 bit characters	
        actual_termios_.c_cc[VMIN] = 1;
        actual_termios_.c_cc[VTIME] = 0;

        if(cfsetispeed(&actual_termios_, speed) < 0 || cfsetospeed(&actual_termios_, speed) < 0)
	    {
            throw RoboclawError(3,"unable to get input and/or output baud rate",0);
    	}	

    	if(tcsetattr(device_handle_, TCSAFLUSH, &actual_termios_) < 0)
	    {
            throw RoboclawError(4,"unable to set the termios structure",0);
	    }
		cout << "Connexion to device succed" << endl;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return ROBOCLAW_ERROR;
    }
    return ROBOCLAW_OK;

}

/**
 * @brief Method to get the status (0 is OK, -1 otherwise)
 * 
 */
int Roboclaw::get_status()
{
    return status_;
}

/**
 * @brief Method to convert a part of a buffer into uint16_t
 * 
 * @param buffer pointer of the buffer
 */
uint16_t Roboclaw::decodeUint16(unsigned char *buffer)
{
    uint16_t value = (uint16_t)(buffer[0])<<8 | (uint16_t)(buffer[1]);
	return value;
}

/**
 * @brief Method to convert a part of a buffer into uint32_t
 * 
 * @param buffer pointer of the buffer
 */
uint32_t Roboclaw::decodeUint32(unsigned char *buffer)
{
    uint32_t value = (uint32_t)(buffer[0])<<24 | (uint32_t)(buffer[1]<<16)
		|(uint32_t)(buffer[2]<<8) | (uint32_t)(buffer[3]) ;
	return value;
}


unsigned int Roboclaw::updateCrc(unsigned char *packet, int nBytes, unsigned int crc)
{
    for (int byte = 0; byte < nBytes; byte++) {
        crc = crc ^ ((unsigned int)packet[byte] << 8);
        for (uint8_t bit = 0; bit < 8; bit++) {
            if ((crc & 0x8000) == 0x8000) {
                crc = (crc << 1) ^ 0x1021;
            } else {
                crc = crc << 1;
            }
        }
    }
    return crc;
}

/**
 * @brief Method to send a simple command to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 */
int Roboclaw::write_(uint8_t address, uint8_t command)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send a 8 bits value to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param value 8 bits value
 */
int Roboclaw::write_1(uint8_t address, uint8_t command, uint8_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send a 16 bits value to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param value 16 bits value
 */
int Roboclaw::write_2(uint8_t address, uint8_t command, uint16_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	buffer_[bytes++] = value>>8;
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send two 16 bits value to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 16 bits values array
 */
int Roboclaw::write_2x2(uint8_t address, uint8_t command, int16_t values[2])
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<2; n++)
	{
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send two unsigned 16 bits value to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 16 bits values array
 */
int Roboclaw::write_2x2u(uint8_t address, uint8_t command, uint16_t values[2])
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<2; n++)
	{
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send a 32 bits value to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param value 32 bits value
 */
int Roboclaw::write_4(uint8_t address, uint8_t command, uint32_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	buffer_[bytes++] = value>>24;
	buffer_[bytes++] = value>>16;
	buffer_[bytes++] = value>>8;
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send 2 32 bits values to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 32 bits value
 */
int Roboclaw::write_2x4(uint8_t address, uint8_t command, int32_t values[2])
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<2; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send two 32 bits value and 8 bits to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 16 bits values array
 * @param value completary 8 bits data
 */
int Roboclaw::write_2x41(uint8_t address, uint8_t command, int32_t values[2], int8_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<2; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send 3 32 bits values to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 32 bits value
 */
int Roboclaw::write_3x4(uint8_t address, uint8_t command, int32_t values[3])
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<3; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send three 32 bits value and 8 bits to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[3] 16 bits values array
 * @param value completary 8 bits data
 */
int Roboclaw::write_3x41(uint8_t address, uint8_t command, int32_t values[3], int8_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<3; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send 4 32 bits values to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[4] 32 bits value
 */
int Roboclaw::write_4x4(uint8_t address, uint8_t command, uint32_t values[4])
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<4; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send four 32 bits value and 8 bits to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[4] 16 bits values array
 * @param value completary 8 bits data
 */
int Roboclaw::write_4x41(uint8_t address, uint8_t command, int32_t values[4], int8_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<4; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}

/**
 * @brief Method to send five 32 bits value and 8 bits to the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[5] 16 bits values array
 * @param value completary 8 bits data
 */
int Roboclaw::write_5x41(uint8_t address, uint8_t command, int32_t values[5], int8_t value)
{
	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	for (int n=0; n<5; n++)
	{
	buffer_[bytes++] = values[n]>>24;
	buffer_[bytes++] = values[n]>>16;
	buffer_[bytes++] = values[n]>>8;
	buffer_[bytes++] = values[n];
	}
	buffer_[bytes++] = value;
	return writeCmdChecksum(bytes);
}


/**
 * @brief Method to read a 16 bits value from the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param value 16 bits value
 */
int Roboclaw::read_2(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint16_t *value)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;

	if (write(device_handle_, buffer_, bytes) < 0) perror("read_2 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_2 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

    if (read(device_handle_, buffer_ , bytes_to_read) < 0) perror("read_2 read");

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);

	calculated_crc = updateCrc(buffer_, bytes_to_read - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		*value = decodeUint16(buffer_);
		return ROBOCLAW_OK;
	}
	else
		return ROBOCLAW_ERROR;
}

/**
 * @brief Method to read two 16 bits value from the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[2] 16 bits values array
 */
int Roboclaw::read_2x2(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint16_t *values[2])
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;

	if (write(device_handle_, buffer_, bytes) < 0) perror("read_2x2 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_42x2 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

	unsigned int readed_bytes=0; //TODO
	if (read(device_handle_, buffer_+readed_bytes, bytes_to_read) < 0)
		perror("read_2x2 - read");	// Read data back from roboclaw

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);
	calculated_crc = updateCrc(buffer_,readed_bytes - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		for (int n=0; n<2; n++)
		{
		*values[n] = decodeUint16(buffer_+ 2*n);
		}
		return ROBOCLAW_OK;
	}
	else
		return ROBOCLAW_ERROR;
}


/**
 * @brief Method to read a 32 bits value from the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param value 32 bits value
 */
int Roboclaw::read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t *value)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;

	if (write(device_handle_, buffer_, bytes) < 0) perror("read_4 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_4 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

	if (read(device_handle_, buffer_, bytes_to_read) < 0)
		perror("read_4 - read");	// Read data back from roboclaw

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);
	calculated_crc = updateCrc(buffer_,bytes_to_read - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		*value = decodeUint32(buffer_);
		return ROBOCLAW_OK;
	}
	else
		return ROBOCLAW_ERROR;
}



/**
 * @brief Method to read a 32 bits value and status (8 bits) from the device
 * @overload
 * @param address address of the board to achieve
 * @param command command to send
 * @param value pointer of a 32 bits value (return)
 * @param status pointer for an additionnal 8 bits info (return)
 */
int Roboclaw::read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t *value, uint8_t *status)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	if (write(device_handle_, buffer_, bytes) < 0) perror("read_4 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_4 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

	if (read(device_handle_, buffer_, bytes_to_read) < 0)
		perror("read_4 - read");	// Read data back from roboclaw

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);
	calculated_crc = updateCrc(buffer_,bytes_to_read - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		*value = decodeUint32(buffer_);
		*status = buffer_[bytes_to_read - ROBOCLAW_CRC16_BYTES -1 ];
		return ROBOCLAW_OK;
	}
	else
	{
		return ROBOCLAW_ERROR;
	}
}


/**
 * @brief Method to read a 32 bits value and status (8 bits) from the device
 * @overload
 * @param address address of the board to achieve
 * @param command command to send
 * @param value pointer of a 32 bits value (return)
 * @param status pointer for an additionnal 8 bits info (return)
 */
int Roboclaw::read_4(uint8_t address, uint8_t command, uint8_t bytes_to_read, uint32_t &value, uint8_t &status)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;
	if (write(device_handle_, buffer_, bytes) < 0) perror("read_4 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_4 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

	if (read(device_handle_, buffer_, bytes_to_read) < 0)
		perror("read_4 - read");	// Read data back from roboclaw

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);
	calculated_crc = updateCrc(buffer_,bytes_to_read - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		value = decodeUint32(buffer_);
		status = buffer_[bytes_to_read - ROBOCLAW_CRC16_BYTES -1 ];
		return ROBOCLAW_OK;
	}
	else
	{
		return ROBOCLAW_ERROR;
	}
}


/**
 * @brief Method to read four 32 bits value from the device
 * 
 * @param address address of the board to achieve
 * @param command command to send
 * @param values[4] 32 bits value
 */
int Roboclaw::read_4x4(uint8_t address, uint8_t command, uint8_t bytes_to_read, std::vector<uint32_t> &values)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = command;

	if (write(device_handle_, buffer_, bytes) < 0) perror("read_4x4 - write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("read_4x4 - tcdrain");
    calculated_crc = updateCrc(buffer_, bytes, 0);

	if (read(device_handle_, buffer_, bytes_to_read) < 0)
		perror("read_4x4 - read");	// Read data back from roboclaw

    unsigned int received_crc = decodeUint16(buffer_ + bytes_to_read - ROBOCLAW_CRC16_BYTES);
	calculated_crc = updateCrc(buffer_,bytes_to_read - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		for (int n=0; n<4; n++)
			values.push_back(decodeUint32(buffer_+ 4*n));
		return ROBOCLAW_OK;
	}
	else
		return ROBOCLAW_ERROR;
}


/**
 * @brief Method to send command and CRC to the device
 * 
 * @param bytes bytes from the buffer_ that have to be written
 */
int Roboclaw::writeCmdChecksum(unsigned int bytes)
{
	unsigned int calculated_crc = updateCrc(buffer_, bytes, 0);
	buffer_[bytes++] = (calculated_crc>>8)&0xFF;
	buffer_[bytes++] = calculated_crc&0xFF;

	if (write(device_handle_, buffer_, bytes) < 0) perror("write error");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("tcdrain error");
	if (read(device_handle_, buffer_, 1) < 0) perror("read error");	// Read data back from USB-ISS, module ID and software version
	if (buffer_[0] != 0xFF)
		return ROBOCLAW_ERROR;
	else
		return ROBOCLAW_OK;
}

/**
 * @brief Method to drive a motor
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param value -127 to 127. 0 full stop, 127 full forward.
 */
int Roboclaw::driveMotor(uint8_t address, uint8_t motor, int8_t value)
{
	if (abs(value)>127)
	{
		cout << "limit exceeded - can't send order";
		return -1;
	}
	switch (motor)
	{
		case (1):
			if (value >=0)
				return write_1(address, M1FORWARD, value);
			else
				return write_1(address, M1BACKWARD, -value);
		case (2):
			if (value >=0)
				return write_1(address, M2FORWARD, value);
			else
				return write_1(address, M2BACKWARD, -value);
	}
	return -1;
}

/**
 * @brief Method to set the min main voltage
 * 
 * cut off to preserve batteries from low levels or over voltage
 * see documentation page 63
 * ideally 2V below the power supply for min
 * and 2V above the power supply for max
 * PREFER METHOD : setBatteryLimitVoltages()
 * 
 * @param address address of the board to achieve
 * @param battery 1 for main; 2 for logic
 * @param type 1 for min limit and 2 for max limit
 * @param value = (Desired Volt. - 6) x 5 for min or 5.12 xDesired Volt for max
 */
int Roboclaw::setBatteryLimitVoltage(uint8_t address, uint8_t battery, uint8_t type, uint8_t value)
{
    if (battery == 1)
	{
		if (type == 1) return write_1(address, SETMINMB, value);
		else if (type == 1) return write_1(address, SETMAXMB, value);
	}
	else if (battery == 2)
	{
		if (type == 1) return write_1(address, SETMINLB, value);
		else if (type == 1) return write_1(address, SETMAXLB, value);
	}
	return -1;
}


/**
 * @brief Method to make a motor moving for- and backward
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param value 0 to 127. 64 full stop, 127 full forward.
 */
int Roboclaw::driveMotor7bit(uint8_t address, uint8_t motor, uint8_t value)
{
	if (value>127)
	{
		cout << "limit exceeded - can't send order";
		return -1;
	}
	switch (motor)
	{
		case (1):
			return write_1(address, M17BIT, value);
		case (2):
			return write_1(address, M27BIT, value);
	}
	return -1;
}

// TODO : COMMAND 8 to 16

/**
 * @brief Method to read encoder
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param encoder_m1 pointer to set with encoder m1 value
 */
int Roboclaw::readEncoder(uint8_t address, uint8_t motor, uint32_t *encoder, uint8_t *status)
{
	switch (motor)
	{
		case (1):
			return read_4(address, GETM1ENC, ROBOCLAW_READ_ENCODERS_REPLY_BYTES, encoder, status);
		case (2):
			return read_4(address, GETM2ENC, ROBOCLAW_READ_ENCODERS_REPLY_BYTES, encoder, status);
	}
	return -1;
}

/**
 * @brief Method to read encoder
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param encoder_m1 pointer to set with encoder m1 value
 */
int Roboclaw::readEncoder(uint8_t address, std::vector<uint32_t> &encoders, std::vector<uint8_t> &encoders_status)
{
	uint32_t encoder;
	uint8_t stat;
	int state = read_4(address, GETM1ENC, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder, stat);
	encoders.push_back(encoder);
	encoders_status.push_back(stat);
	state += read_4(address, GETM2ENC, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder, stat);
	encoders.push_back(encoder);
	encoders_status.push_back(stat);
	return state;
}


/**
 * @brief Method to read encoder speed
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param encoder_speed pointer to get with encoder value
 * @param status pointer to get the status
 */
int Roboclaw::readEncoderSpeed(uint8_t address,uint8_t motor, uint32_t *encoder_speed, uint8_t *status)
{
	if (motor==1)
		return read_4(address, GETM1SPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder_speed, status);
	else if (motor==2)
		return read_4(address, GETM2SPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder_speed, status);
	return -1;
}

/**
 * @brief Method to read encoder speed
 * 
 * @param address address of the board to achieve
 * @param encoder_speed reference to get both encoders value
 * @param status reference to get both encoder the status
 */
int Roboclaw::readEncoderSpeed(uint8_t address, std::vector<uint32_t>& encoders_speed, std::vector<uint8_t>& encoders_status)
{
	uint32_t encoder;
	uint8_t status;
	int state = read_4(address, GETM1ISPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder, status);
	encoders_speed.push_back(encoder);
	encoders_status.push_back(status);
	state += read_4(address, GETM2ISPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder, status);
	encoders_speed.push_back(encoder);
	encoders_status.push_back(status);
	return state;
}



/**
 * @brief Method to reset encoder counters
 * 
 * @param address address of the board to achieve
 */
int Roboclaw::resetEncoders(uint8_t address)
{
	return write_(address, RESETENC);
}

/**
 * @brief Method to reset encoder counters
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param value to overwrite the counter
 */
int Roboclaw::setEncoderValue(uint8_t address, uint8_t motor, uint32_t value)
{
	switch (motor)
	{
		case (1):
			return write_4(address, SETM1ENCCOUNT, value);
		case (2):
			return write_4(address, SETM2ENCCOUNT, value);
	}
	return -1;
}


/**
 * @brief Method to display the product name and the firmware version
 * 
 * @param address address of the board to achieve
 */
int Roboclaw::displayVersion(uint8_t address)
{
	unsigned int calculated_crc;

	unsigned int bytes = 0;
	buffer_[bytes++] = address;
	buffer_[bytes++] = GETVERSION;

    // send command to the device
	//if (write(*(int *)serial_, buffer_, bytes) < 0) perror("displayVersion write");	// Write data to device
    //if (tcdrain(*(int *)serial_) < 0) perror("displayVersion tcdrain");
	if (write(device_handle_, buffer_, bytes) < 0) perror("displayVersion write");	// Write data to device
    if (tcdrain(device_handle_) < 0) perror("displayVersion tcdrain");

    // calculate CRC with the send command
    calculated_crc = updateCrc(buffer_, bytes, 0);

    // read data from the device
	char version[ROBOCLAW_READ_VERSION];
	unsigned int readed_bytes;
    for (readed_bytes=1; readed_bytes < ROBOCLAW_READ_VERSION; readed_bytes++)
	{
		//if (read(*(int *)serial_, buffer_ + readed_bytes - 1, 1) < 0)
		if (read(device_handle_, buffer_ + readed_bytes - 1, 1) < 0)
		{
			perror("displayVersion read");	// Read data back from roboclaw
			break;
		}

		version[readed_bytes-1]=(char)( buffer_[readed_bytes-1]);
		// Version is up to 48 characters ended with 10 and 0 
		if (readed_bytes > 2)
		    if (buffer_[readed_bytes-3] == 0 && buffer_[readed_bytes-4] == 10)
    			break;
	}

    unsigned int received_crc = decodeUint16(buffer_ + readed_bytes - 2 );
	calculated_crc = updateCrc(buffer_,readed_bytes - ROBOCLAW_CRC16_BYTES, calculated_crc);

	if ( (calculated_crc&0xFFFF) == (received_crc&0xFFFF))
	{
		cout << "Produit et firmware : " <<  std::string(version) << endl;
		return ROBOCLAW_OK;
	}
	else
		return ROBOCLAW_ERROR;
}


/**
 * @brief Method to read main or logic battery level
 * 
 * @param address address of the board to achieve
 * @param battery 1 for main, 2 for logic
 * @param voltage pointer to get the voltage (in 10ths of a volt)
 */
int Roboclaw::readBatteryLevel(uint8_t address, uint8_t battery, uint16_t *voltage)
{
	switch (battery)
	{
		case (1):
			return read_2(address, GETMBATT, ROBOCLAW_READ_MAIN_BATTERY_REPLY_BYTES, voltage);
		case (2):
			return read_2(address, GETLBATT, ROBOCLAW_READ_MAIN_BATTERY_REPLY_BYTES, voltage);
	}
	return -1;
}


/**
 * @brief Method to set velocity PID constats
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param P proportional coeff
 * @param I integral coeff
 * @param D derivative coeff
 * @param QPPS speed of the encoder when the motor is at 100% power
 */
int Roboclaw::setVelocityPIDConstants(uint8_t address, uint8_t motor, uint32_t P, uint32_t I, uint32_t D, uint32_t QPPS)
{
	uint32_t values[4] = {D, P, I, QPPS};
	switch (motor)
	{
		case (1):
			return write_4x4(address, SETM1PID, values);
		case (2):
			return write_4x4(address, SETM2PID, values);
	}
	return -1;
}

/**
 * @brief Method to read raw speed (unfiltered command of 18 and 19)
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor1, 2 for motor2
 * @param raw_speed pointer to get the raw speed
 * @param status pointer to get the status
 */
int Roboclaw::readRawSpeed(uint8_t address,uint8_t motor, uint32_t *encoder_speed, uint8_t *status)
{
	switch (motor)
	{
		case (1):
			return read_4(address, GETM1ISPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder_speed, status);
		case (2):
			return read_4(address, GETM2ISPEED, ROBOCLAW_READ_ENCODERS_SPEED_REPLY_BYTES, encoder_speed, status);
	}
	return -1;
}

/**
 * @brief Method to drive a motor with a duty cycle (encoder not needed)
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for meadotor 2
 * @param value -32767 to 32676 (-100% to 100%)
 */
int Roboclaw::driveMotorWithDutyCycle(uint8_t address, uint8_t motor, int16_t value)
{
	switch (motor)
	{
		case (1):
			return write_2(address, M1DUTY, value);
		case (2):
			return write_2(address, M2DUTY, value);
	}
	return -1;
}

/**
 * @brief Method to drive both motor with a duty cycle (encoder not needed)
 * 
 * @param address address of the board to achieve
 * @param duty_M1 -32767 to 32676 (-100% to 100%) for motor 1
 * @param duty_M2 -32767 to 32676 (-100% to 100%) for motor 2
 */
int Roboclaw::driveMotorsWithDutyCycle(uint8_t address, int16_t duty_M1, int16_t duty_M2)
{
	int16_t values[2] = {duty_M1, duty_M2};
	return write_2x2(address, MIXEDDUTY, values);
}

/**
 * @brief Method to drive a motor with qpps (encoder needed)
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param speed signed speed in qpps (quadrature pulse per second)
 */
int Roboclaw::driveMotorWithSpeed(uint8_t address, uint8_t motor, int32_t speed)
{
	switch (motor)
	{
		case (1):
			return write_4(address, M1SPEED, speed);
		case (2):
			return write_4(address, M2SPEED, speed);
	}
	return -1;
}

/**
 * @brief Method to drive both motor with qpps (encoder needed)
 * 
 * @param address address of the board to achieve
 * @param speed_M1 signed speed (qpps) of motor 1
 * @param speed_M2 signed speed (qpps) of motor 2
 */
int Roboclaw::driveMotorsWithSpeed(uint8_t address, int32_t speed_M1, int32_t speed_M2)
{
	int32_t values[2] = {speed_M1, speed_M2};
	return write_2x4(address, MIXEDSPEED, values);
}

/**
 * @brief Method to drive a motor with qpps and acceleration (encoder needed)
 * 
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param speed signed speed in qpps (quadrature pulse per second)
 * @param accel in qpps (see page 93 of roboclaw user manual)
 */
int Roboclaw::driveMotorWithSpeedAndAccel(uint8_t address, uint8_t motor, int32_t speed, int32_t accel)
{
	int32_t values[2] = {accel, speed};
	switch (motor)
	{
		case (1):
			return write_2x4(address, M1SPEEDACCEL, values);
		case (2):
			return write_2x4(address, M2SPEEDACCEL, values);
	}
	return -1;
}

/**
 * @brief Method to drive both motors with qpps and acceleration (encoder needed)
 * 
 * @param address address of the board to achieve
 * @param speed_M1 signed speed in qpps (quadrature pulse per second)
 * @param speed_M2 signed speed in qpps (quadrature pulse per second)
 * @param accel in qpps (see page 93 of roboclaw user manual)
 */
int Roboclaw::driveMotorsWithSpeedAndAccel(uint8_t address, int32_t speed_M1, int32_t speed_M2, int32_t accel)
{
	int32_t values[3] = {accel, speed_M1, speed_M2};
	return write_3x4(address, MIXEDSPEEDACCEL, values);
}

/**
 * @brief Method to drive a motor with qpps and distance (encoder needed)
 * see page 94
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param speed signed speed in qpps (quadrature pulse per second)
 * @param distance motor travel distance (quadrature pulse)
 * @param buffer 0 to put the command in the buffer, 1 to stop the running command and execute this new command
 */
int Roboclaw::driveMotorWithSpeedAndDist(uint8_t address, uint8_t motor, int32_t speed, int32_t distance, int8_t buffer)
{
	int32_t values[2] = {speed, distance};
	switch (motor)
	{
		case (1):
			return write_2x41(address, M1SPEEDDIST, values, buffer);
		case (2):
			return write_2x41(address, M2SPEEDDIST, values, buffer);
	}
	return -1;
}

/**
 * @brief Method to drive both motors with qpps and distance (encoder needed)
 * see page 94
 * @param address address of the board to achieve
 * @param speed_M1 signed speed in qpps (quadrature pulse per second) for motor 1
 * @param distance_M1 motor travel distance (quadrature pulse) for motor 1
 * @param speed_M2 signed speed in qpps (quadrature pulse per second) for motor 2
 * @param distance_M2 motor travel distance (quadrature pulse) for motor 2
 * @param buffer 0 to put the command in the buffer, 1 to stop the running command and execute this new command
 */
int Roboclaw::driveMotorsWithSpeedAndDist(uint8_t address, int32_t speed_M1, int32_t distance_M1, int32_t speed_M2, int32_t distance_M2, int8_t buffer)
{
	int32_t values[4] = {speed_M1, distance_M1, speed_M2, distance_M2};
	return write_4x41(address, MIXEDSPEEDDIST, values, buffer);
}

/**
 * @brief Method to drive a motor with qpps, accel and distance (encoder needed)
 * see page 95
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param speed signed speed in qpps (quadrature pulse per second)
 * @param accel signed accel in qpps
 * @param distance motor travel distance (quadrature pulse)
 * @param buffer 0 to put the command in the buffer, 1 to stop the running command and execute this new command
 */
int Roboclaw::driveMotorWithSpeedAccelAndDist(uint8_t address, uint8_t motor, int32_t speed, int32_t accel, int32_t distance, int8_t buffer)
{
	int32_t values[3] = {accel, speed, distance};
	switch (motor)
	{
		case (1):
			return write_3x41(address, M1SPEEDACCELDIST, values, buffer);
		case (2):
			return write_3x41(address, M2SPEEDACCELDIST, values, buffer);
	}
	return -1;
}

/**
 * @brief Method to drive both motors with speed, accel and distance (encoder needed)
 * see page 94
 * @param address address of the board to achieve
 * @param accel in qpps
 * @param speed_M1 signed speed in qpps (quadrature pulse per second) for motor 1
 * @param distance_M1 motor travel distance (quadrature pulse) for motor 1
 * @param speed_M2 signed speed in qpps (quadrature pulse per second) for motor 2
 * @param distance_M2 motor travel distance (quadrature pulse) for motor 2
 * @param buffer 0 to put the command in the buffer, 1 to stop the running command and execute this new command
 */
int Roboclaw::driveMotorsWithSpeedAccelAndDist(uint8_t address, int32_t speed_M1, int32_t distance_M1, int32_t speed_M2, int32_t distance_M2, int32_t accel, int8_t buffer)
{
	int32_t values[5] = {accel, speed_M1, distance_M1, speed_M2, distance_M2};
	return write_5x41(address, MIXEDSPEEDACCELDIST, values, buffer);
}

// TODO : COMMAND 52 to 54


/**
 * @brief Method to read PID values of a motor
 * see page 98
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param PID[4] array of uint32_t with P, I, D and QPPS
 */
int Roboclaw::readVelocityPID(uint8_t address, uint8_t motor, std::vector<uint32_t> &PID)
{
	switch (motor)
	{
		case (1):
			return read_4x4(address, READM1PID, ROBOCLAW_READ_SPEED_PID_REPLY_BYTES, PID);
		case (2):
			return read_4x4(address, READM2PID, ROBOCLAW_READ_SPEED_PID_REPLY_BYTES, PID);
	}
	return -1;
}


/**
 * @brief Method to set the min main voltage
 * 
 * cut off to preserve batteries from low levels or over voltages
 * see documentation page 69
 * ideally 2V below the power supply for min
 * and 2V above the power supply for max
 * 
 * @param address address of the board to achieve
 * @param battery 1 for main; 2 for logic
 * @param min_voltage = Desired Volt. x 10 
 * @param max_voltage = Desired Volt. x 10 
 */
int Roboclaw::setBatteryLimitVoltages(uint8_t address, uint8_t battery, uint16_t min_voltage, uint16_t max_voltage)
{
	uint16_t values[2] = {min_voltage, max_voltage};
    if (battery == 1)
	{
		return write_2x2u(address, SETMAINVOLTAGES, values);		
	}
	else if (battery == 2)
	{
		return write_2x2u(address, SETLOGICVOLTAGES, values);		
	}
	return -1;
}

/**
 * @brief Method to read battery min and max voltages
 * see page 69
 * @param address address of the board to achieve
 * @param motor 1 for motor 1, 2 for motor 2
 * @param limit_voltages[2] array of uint12_t with min and max settings
 */
int Roboclaw::readBatteryLimitVoltages(uint8_t address, uint8_t battery, uint16_t *limit_voltages[2])
{
	switch (battery)
	{
		case (1):
			return read_2x2(address, GETMINMAXMAINVOLTAGES, ROBOCLAW_READ_LIMIT_VOLTAGES_REPLY_BYTES, limit_voltages);
		case (2):
			return read_2x2(address, GETMINMAXMAINVOLTAGES, ROBOCLAW_READ_LIMIT_VOLTAGES_REPLY_BYTES, limit_voltages);
	}
	return -1;
}

/**
 * @brief Method to close communication through
 * 
 */
void Roboclaw::closeCommunication(void)
{
    close(device_handle_);
}
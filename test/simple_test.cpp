#include "ros/ros.h"
#include "roboclaw/roboclaw.hpp"
#include <boost/timer.hpp>
#include <ros/console.h>

int main( int argc, char *argv[])
{
    
    ros::init(argc, argv, "roboclaw");
    ros::NodeHandle ros_nh;

    unsigned int address = 0x80;
    Roboclaw roboclaw_network {"/dev/roboclaw",115200};
    roboclaw_network.displayVersion(address);
    uint16_t *voltage = new uint16_t;
    roboclaw_network.readBatteryLevel(address,1,voltage);
    ROS_INFO("Tension batterie : %f", *voltage/10.0);
    roboclaw_network.setBatteryLimitVoltage(address,1,2,69);

    ros::Rate ros_loop(10);

    ros::Time ros_begin_time = ros::Time::now();
    ros::Duration duration = ros_begin_time - ros::Time::now();
    roboclaw_network.driveMotor(address,2,20);
    while (ros::ok() && duration.toSec() < 10.0)
    {
        uint32_t *encoder_M2 = new uint32_t;
        uint8_t *status = new uint8_t;
        roboclaw_network.readEncoder(address,2,encoder_M2, status);
        ROS_INFO("encodeur : %i",*encoder_M2);
        ros_loop.sleep();
        duration = ros_begin_time - ros::Time::now();
    }
    roboclaw_network.driveMotor(address,2,0);


    return 0;
}